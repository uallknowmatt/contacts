module.exports = {
	InsertContact: function (req, res, pool) {
		pool.connect(function (err, keyspace) {
			if (err) {
				throw(err);
			} else {
				var post = req.body;
				pool.execute("INSERT INTO CONTACTS.contact (email, name, phone, address, city) VALUES (?,?,?,?,?)", [post.email, post.name, post.phone, post.address, post.city], function (err, results) {
					res.redirect('/');
				});
			}
		});
	},
	UpdateContact: function (req, res, pool) {
		pool.connect(function (err, keyspace) {
			if (err) {
				throw(err);
			} else {
				var post = req.body;
				pool.execute("UPDATE CONTACTS.contact SET name = ?, phone = ?, address = ?, city = ? WHERE email = ?", [post.name, post.phone, post.address, post.city, post.contactEdit], function (err, results) {
					res.redirect('/');
				});
			}
		});
	},
	DeleteContact: function (req, res, pool) {
		pool.connect(function (err, keyspace) {
			if (err) {
				throw(err);
			} else {
				var post = req.body;
				pool.execute("DELETE FROM CONTACTS.contact WHERE email = ?", [post.contactDel], function (err, results) {
					res.redirect('/');
				});
			}
		});
	},
	LoadContacts: function (req, res, pool) {
		var data =[];
		pool.connect(function (err, keyspace) {
			if (err) {
				throw(err);
			} else {
				var post = req.body;
				var query = "SELECT * FROM CONTACTS.contact";
				if (typeof(req.query.email) != "undefined") {
					query += " WHERE email = '" + req.query.email + "'";
				}
				pool.stream(query)
					.on('readable', function () {
						// readable is emitted as soon a row is received and parsed
						var row;
						while (row = this.read()) {
							data.push(row);
						}
					})
					.on('end', function () {
						res.send(data);
					});
			}

		});
	}
}