STEPS TO RUN THIS APPLICATION.
==============================
INSTALL CASSANDRA FROM PLANET CASSANDRA

go to cassandra directory and in ./bin directory
run ./cqlsh

run cassandra-scripts.sql in cqlsh
At this point the table will be created in cassandra

http://www.planetcassandra.org/cassandra/

run npm install

`npm install`

After...

`node app.js`

## Result 

Open your browser to link `http://localhost:8181/`

